/**
 *
 */
package de.fhbingen.epro;

import java.util.EnumSet;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import de.fhbingen.epro.cors.CORSFilter;

/**
 *
 * @author Johannes Hiemer.
 *
 */
public class CustomWebInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext rootCtx = new AnnotationConfigWebApplicationContext();
		rootCtx.register(...);

		servletContext.addListener(new ContextLoaderListener(rootCtx));

		servletContext.addFilter("corsFilter", CORSFilter.class);
		servletContext.getFilterRegistration("corsFilter").addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),
				false, "/*");

		AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
		webCtx.setParent(rootCtx);
		webCtx.setServletContext(servletContext);
		webCtx.register(CustomMvcConfiguration.class);

		DispatcherServlet dispatcherServlet = new DispatcherServlet(webCtx);
		ServletRegistration.Dynamic appServlet = servletContext.addServlet("exporter", dispatcherServlet);
		appServlet.setAsyncSupported(true);
		appServlet.setLoadOnStartup(1);
		appServlet.addMapping("/");
	}

}
